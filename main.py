from pyrogram import Client, filters
import check
import json

with open('config.json', 'r') as file:
    config_data = file.read()

with open('antibot.json', 'r') as file:
    bot_conf_data = file.read()

config = json.loads(config_data)
bot_conf = json.loads(bot_conf_data)

api_id = int(bot_conf['api_id'])
api_hash = str(bot_conf['api_hash'])
session = str(bot_conf['session'])

app = Client(session, api_id, api_hash)


@app.on_message(filters.regex(pattern=r'^\/m .*$') & filters.private)
async def morphy(client, message):
    await message.reply(check.morphy_word(message.text.replace('/m ', '')))


def gen_reason(rate: dict) -> str:
    reason = ''
    reason += f'emoji        {rate["emoji_rate"]}\n'
    reason += f'link         {rate["link_rate"]}\n'
    reason += f'tag          {rate["tag_rate"]}\n'
    if rate["word_rate"] != 0:
        reason += f'words        {rate["word_rate"]}\n'
    if rate["emoji_word_rate"] != 0:
        reason += f'emoji words  {rate["emoji_word_rate"]}\n'
    return reason


@app.on_message()
async def anti_bot(client, message):
    reason = ''
    rate = 0
    name = f'{message.from_user.first_name if message.from_user.first_name else ""} ' \
           f'{message.from_user.last_name if message.from_user.last_name else ""}'
    if str(message.chat.id) not in config.keys():
        chat_conf = {
                        "ban": False,
                        "send_reason": True,
                        "tag_admins": False,
                        "admins": [],
                        "max_f": 5
                    }
    else:
        chat_conf = config[str(message.chat.id)]
    bio = await app.get_chat(message.from_user.id)
    bio = bio.bio
    print(f'message text: {message.text}')
    print(f'user bio:     {bio}')
    print(f'user name:    {name}')

    if message.text:
        message_rate = check.check_message(message.text)
        reason += 'Message rate:\n'
        reason += gen_reason(message_rate)
        rate += message_rate["emoji_rate"]
        rate += message_rate["link_rate"]
        rate += message_rate["tag_rate"]
        rate += message_rate["word_rate"]
        rate += message_rate["emoji_word_rate"]

    if bio:
        bio_rate = check.check_message(bio)
        reason += 'bio rate:\n'
        reason += gen_reason(bio_rate)
        rate += bio_rate["emoji_rate"]
        rate += bio_rate["link_rate"]
        rate += bio_rate["tag_rate"]
        rate += bio_rate["word_rate"]
        rate += bio_rate["emoji_word_rate"]

    if name:
        name_rate = check.check_message(name)
        reason += 'name rate:\n'
        reason += gen_reason(name_rate)
        rate += name_rate["emoji_rate"]
        rate += name_rate["link_rate"]
        rate += name_rate["tag_rate"]
        rate += name_rate["word_rate"]
        rate += name_rate["emoji_word_rate"]

    messages_from_user = app.search_messages(message.chat.id, "+", from_user=message.from_user.id, limit=5)
    messages_from_user = [m async for m in messages_from_user]
    if messages_from_user:
        messages_from_user = len(messages_from_user)
    else:
        messages_from_user = 0
    # try:
    #     messages_from_user = len(app.search_messages(message.chat.id, "+", from_user=message.from_user.id, limit=5))
    # except:
    #     messages_from_user = 0
    print(f'messages_from_user: {messages_from_user}')

    # TODO: Rewrite with match/case from python 3.10
    new_user_factor = 1
    if messages_from_user >= 5:
        new_user_factor = 0.2
    elif messages_from_user >= 3:
        new_user_factor = 0.5
    elif messages_from_user <= 1:
        new_user_factor = 1.5
    rate = rate*new_user_factor

    reason += f'user factor  *{new_user_factor}\n'
    reason += f'sum          {rate}\n'
    reason += f'is a bot? {rate >= chat_conf["max_f"]}'

    if rate >= chat_conf['max_f']:
        await message.forward('me')

        if chat_conf['send_reason']:
            await message.reply(reason)
        if chat_conf['tag_admins']:
            await message.reply(f"@{admin} " for admin in chat_conf['admins'])
        await app.send_message('me', reason)

    print(reason)


app.run()
