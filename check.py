import re
import demoji
import settings
import pymorphy2

morph = pymorphy2.MorphAnalyzer()


def morphy_word(word: str) -> str:
    word = word.lower()
    for a, b in settings.chars.items():
        word = word.replace(a, b)
    return morph.parse(word)[0].normal_form


def check_message(text: str) -> dict:
    rate = 0

    text = text.lower()

    emojis = demoji.findall(text)
    emoji_rate = len(emojis) * settings.emoji_weight
    for emoji, emoji_text in emojis.items():
        text = text.replace(emoji, f' {emoji_text} ')

    link_find = re.findall(r'((http|http):\/\/|)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])',
                           text)
    link_rate = 0
    if len(link_find):
        link_rate = settings.link_weight

    tag_find = re.findall(r'(?:\@[\w_-]+)', text)
    tag_rate = 0
    if len(tag_find):
        tag_rate = settings.tag_weight

    text = re.sub(r'[^\w\s]', '', text)

    emoji_word_rate = 0
    word_rate = 0
    text = text.replace('⠀', ' ')
    for word in text.split(' '):
        for bad_word in settings.bad_emoji_words:
            if bad_word in word:
                emoji_word_rate += settings.emoji_word_weight
        morph_word = morphy_word(word)
        if morph_word in settings.words.keys():
            word_rate += settings.words[morph_word]

    return {'emoji_rate': emoji_rate, 'link_rate': link_rate, 'tag_rate': tag_rate,
            'emoji_word_rate': emoji_word_rate, 'word_rate': word_rate}

