import json
import time
import random
from pyrogram import Client

test_texts = [
    'Teбe xoчется гopячиx 💘 вcтpeч c дeвyшkaми? Oни ждyт тeбя здecь 😉 👉 https://t.me/aviationmode',
    'Лёгкий способ найди дpузей для секcа 💘 рядом с тобой 😏👉 https://t.me/aviationmode',
    '🔥Kaнaл пo Дapkнeтy - @aviationmode\n🔥Kaнaл пo Дapkнeтy - @aviationmode\n🔥Kaнaл пo Дapkнeтy - @aviationmode',
    'Хочешь быстрых потpaхушек? Заходи скорей сюда 😉👉 https://t.me/aviationmode',
    '💋ЗНАКОМСТВА ДЛЯ СЕКСА - https://t.me/aviationmode',
    'Лёгкий cпocoб найти дeвушeк для взpocлыx шaлocтeй 💦  https://t.me/aviationmode'
]

test_bios = [
    'Xочешь нoвых ощущeний?🔥 Переxoди по ccылke😉 https://t.me/aviationmode',
    'Пoнравилaсь? Пиши! ❤️ Мой прoфиль https://t.me/aviationmode'
]
test_bios_texts = [
    '😉',
    '😘'
]

with open('./tester.json', 'r') as file:
    data = file.read()

obj = json.loads(data)

api_id = int(obj['api_id'])
api_hash = str(obj['api_hash'])
session = str(obj['session'])


def test_message():
    with Client(session, api_id, api_hash) as app:
            app.update_profile(bio='')
            app.send_message('antispam_botik', test_texts[random.randint(0, len(test_texts)-1)])
            time.sleep(10)
            history = app.get_history('antispam_botik', limit=5)
            message_text = history[0].text
            app.delete_messages('antispam_botik', [i.message_id for i in history])
            assert 'is a bot? True' in message_text


def test_bio():
    with Client(session, api_id, api_hash) as app:
        app.update_profile(bio=test_bios[random.randint(0, len(test_bios)-1)])
        app.send_message("antispam_botik", test_bios_texts[random.randint(0, len(test_bios_texts)-1)])
        time.sleep(10)
        history = app.get_history('antispam_botik', limit=5)
        message_text = history[0].text
        app.delete_messages('antispam_botik', [i.message_id for i in history])
        app.update_profile(bio='')
        assert 'is a bot? True' in message_text


def test_morphy():
    with Client(session, api_id, api_hash) as app:
        app.send_message('antispam_botik', '/m теcтoм')
        time.sleep(10)
        history = app.get_history('antispam_botik', limit=5)
        message_text = history[0].text
        app.delete_messages('antispam_botik', [i.message_id for i in history])
        assert 'тест' == message_text


def test_name():
    with Client(session, api_id, api_hash) as app:
        app.update_profile(first_name="Секс", last_name="Знакомства")
        app.send_message('antispam_botik', '123')
        time.sleep(10)
        history = app.get_history('antispam_botik', limit=5)
        message_text = history[0].text
        app.delete_messages('antispam_botik', [i.message_id for i in history])
        app.update_profile(bio='')
        app.update_profile(first_name="Тест", last_name="Тест")
        assert 'is a bot? True' in message_text
