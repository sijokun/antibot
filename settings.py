words = {
    'секс': 4,
    'кекс': 4,
    'даркнета': 4,
    'любовь': 1,
    'отношения': 2,
    'знакомиться': 2,
    'обязательство': 1,
    'анкета': 1,
    'взлом': 2,
    'хакер': 2,
    'hack': 3,
    'девушка': 1,
    'горячий': 1,
    'по-взpослому': 2,
    'недетский': 1,
    'искать': 1,
    'чпокать': 3,
    'шалить': 2,
    'голый': 2,
    'потрахушка': 3,
    'шалость': 1
}
bad_emoji_words = ['heart', 'kiss', 'sex', 'fire']
chars = {
    'x': 'х',
    'c': 'с',
    'b': 'в',
    'a': 'а',
    't': 'т',
    'e': 'е',
    'o': 'о',
    'k': 'к',
    'm': 'м',
    'h': 'н',
    'y': 'у',
    'p': 'р',
    '0': 'о',
    '⠀': ' ',
    'n': 'п',
    '4': 'ч',
    '.': '',
    '?': '',
    ',': '',
    ':': '',
    '"': '',
    '///': 'ж',
    's': 'с'
}
emoji_word_weight = 1
emoji_weight = 0.5
link_weight = 2
tag_weight = 1

